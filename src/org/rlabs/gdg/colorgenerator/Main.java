package org.rlabs.gdg.colorgenerator;

import java.util.Random;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Main extends Activity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupView();
	}

	private void setupView() {
		// Info: The code will continue from here

		// This button button links what is on your screen, the button you see
		// with something called a Listener.
		// A listener is a class in java that wait for something to happens, but
		// you got to ask it to listen to something first.
		Button button1 = (Button) findViewById(R.id.button1);
		// Here were asking the listener to listen for a button press
		button1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// Here we want to change the Background color of a TextView we declared
		// in our XML. Below we first declare the TextView so that Java can use
		// it to change later
		TextView textView1 = (TextView) findViewById(R.id.textView1);
		// Here we referencing a function to call new colors and store it in a
		// class called randomColor
		int randomColor = getColor();
		// Since we have the TextView can can tell it to change the color we
		// stored in the variable called 'randomColor'. So we pass it to the
		// TextView by setting the Background color.
		textView1.setBackgroundColor(randomColor);
	}

	private int getColor() {
		// Since we doing Random things. We create a class called Random. Random
		// exists already as a default library in the Java files. To get
		// anything random we use ...Random :)
		Random rnd = new Random();
		// With random you can tell it to pick a number between 1 - 256. In this
		// case to generator a color. We have to use
		int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256),
				rnd.nextInt(256));
		return color;
	}

}
